modules.define('command', ['arrays'],
function(provide, Arrays) {

    function apply(state, Func, key, value) {
        var initialArray = state[key] || [];
        var resultArray = Func(initialArray, value);

        if (resultArray.length === initialArray.length) {
            return {}; // оптимизация: если ничего не изменилось, то и не надо ничего записывать
        }

        var changedProps = {};
        changedProps[key] = resultArray;
        return changedProps;
    }

    const actions = {
        setMany(value) {
            return value;
        },

        set(value, key) {
            var changedProps = {};
            changedProps[key] = value;
            return changedProps;
        },

        add(value, key, state) {
            return apply(state, Arrays.pushUnique.bind(Arrays), key, value);
        },

        remove(value, key, state) {
            return apply(state, Arrays.remove.bind(Arrays), key, value);
        }
    };

    /**
     * Command - это закодированная для передачи по шине команда,
     * action - это функция, результат которой набор фактических изменений состояния
     */
    class Command {
        transformRequest(command, state) {
            const action = this.actions[command.action];
            if (!action) {
                console.warn('игнорируем неизвестную команду:', command);
                return {};
            }
            const changedProps = action(command.value, command.key, state);
            return changedProps;
        }

        transformResponse(response) {
            return response;
        }
    }

    Command.prototype.actions = actions;

    provide(Command);
}
);
