({ shouldDeps: [
    'storage',
    'request-bus', 'response-bus',
    'command', 'dates-model',
    { block: 'functions', elem: 'debounce' }
] })
