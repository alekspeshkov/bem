modules.define('model-adapter', ['storage', 'request-bus', 'response-bus', 'command', 'dates-model', 'functions__debounce'],
function(provide, stateStorage, requestBus, responseBus, Command, DatesModel, debounce) {

    let models = [];
    models.push(new DatesModel());
    models.push(new Command()); //должна быть последней

    /**
     * Эта функция выполняется в ответ на специальный пустой request.
     * Такой request должен отправить каждый новый слушатель модели,
     * т.е при старте приложения почти сразу же будет создано множество слушателей
     * и таких одинаковых запросов будет создано очень много практически одновременно,
     * а потом (скорее всего) больше вообще не будет.
     *
     * Чтобы немного разгрузить процесс начальной иниициализации,
     * мы защитим данную функцию c помощью debounсe(),
     * т.к. на все запросы одинаковые начальные запросы
     * response совершенно одинаковый и его одного вполне достаточно на всех.
     */
    var pingResponse = debounce( function() {
        stateStorage.pingState();
    }, 20);

    var sendResponse = function(state, changedKeys) {
        /**
         * собственно это и есть формат response сообщения
         **/
        var response = {
            state: state,
            changedKeys: changedKeys
        };

        models.forEach( function(model) {
            response = model.transformResponse(response);
        });

        responseBus.send(response);
    };

    var handleCommand = function(command) {
        /**
         * особая команда, которую нужно отдельно обрабатывать
         **/
        if (command.action === 'ping') {
            pingResponse();
            return;
        }

        const state = stateStorage.cloneState();

        /**
         * здесь мы делаем то, что должна делать бизнес-логика
         * до записи в State
         **/
        models.forEach( function(model) {
            command = model.transformRequest(command, state);
        });

        /**
         * собственно запись в State переработанного request
         **/
        stateStorage.updateState(command);
    };

    var init = function() {
        /**
         * подписываемся на события изменения State
         **/
        stateStorage.subscribe(sendResponse.bind(this));

        /**
         * подписываемся на сообщения из шины request
         **/
        requestBus.subscribe(handleCommand.bind(this));

        /**
         * Модель сообщает всем слушателям созданным раньше чем сама модель,
         * что она родилась
         **/
        pingResponse();
    };

    init();
    provide();
});
