modules.define('message-bus',
function(provide, Base) {

    /**
     * @param instanceName необязательное имя конкретного экземпляра, нужно только для отладочных console.log() сообщений
     **/
    class MessageBus extends Base {
        constructor(instanceName) {
            super();
            this._name = instanceName;
        }

        send(...args) {
            this.log('.send', args);
            super.send.apply(this, args);
        }

        subscribe(subscriber) {
            const key = super.subscribe(subscriber);
            this.log('.subscribe', key);
            return key;
        }

        unsubscribe(key) {
            this.log('.unsubscribe', key);
            super.unsubscribe(key);
        }

        log(text, args) {
            this._name && console.log('' + this._name + text, args || '');
        }
    }

    provide(MessageBus);

});
