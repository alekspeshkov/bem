modules.define('countries-select', ['input', 'countries-data', 'arrays', 'dom', 'functions__debounce', 'softmatch', 'i-bem-dom', 'BEMHTML'],
function(provide, Input, CountriesData, Arrays, DOM, debounce, SoftMatch, bemDom, BEMHTML) { provide(bemDom.declBlock(this.name,
{
    onSetMod: {
        js: {
            inited() {
                this.inputTextBlock = this.findChildBlock(Input);

                this.selectedCountries = []; //уже выбранные страны иключаются из выбора (изначально ничего)
                this.visibleCountries = []; //все выбранные страны на данный момент
                this.filteredCountries = []; //список отфильтрованных по введенным буквам стран
                this.clearFilter();

                var specialKeys = {
                    '13': function() { this.submit(this.inputTextBlock.getVal()); }.bind(this),
                    '38': function() { console.warn('prev not implemented'); },
                    '40': function() { console.warn('next not implemented'); }
                };

                var debounceSetFilter = debounce( function() {
                    this.setFilter(this.inputTextBlock.getVal());
                }.bind(this), 100);

                this._domEvents(this.inputTextBlock).on('keyup', function(e) {
                    var key = (e.keyCode || e.which);
                    if (specialKeys[key]) {
                        return specialKeys[key](e);
                    }

                    debounceSetFilter();
                }.bind(this));
            }
        }
    },

    render(codes) {
        const names = (codes || []).map(CountriesData.getName);
        this.selectedCountries = names;
        this.redraw();
    },

    redraw() {
        const visibleCountries = Arrays.diff(this.filteredCountries, this.selectedCountries);

        if (Arrays.equal(visibleCountries, this.visibleCountries)) {
            return;
        }
        this.visibleCountries = visibleCountries;

        const parentElem = this.findChildElem('countries-list').domElem.get(0); //необходимо найти динамически
        this.__self.replaceChildren(parentElem, visibleCountries);
    },

    _onClick(e) {
        const name = e.target.textContent.trim();
        this.selectCountry(name);
    },

    selectCountry(name) {
        const code = CountriesData.getCode(name);
        if (code) {
            this._emit('select', code);
            this.clearFilter();
        }
    },

    clearFilter() {
        this.inputTextBlock.setVal('');

        this.lastText = '';
        this.filteredCountries = this.__self._names;
    },

    setFilter(text) {
        if (text == this.lastText) {
            return;
        }

        this.lastText = text;
        this.filteredCountries = this.__self.filterCountries(this.lastText);

        this.redraw();
    },

    submit(text) {
        this.lastText = text;
        this.filteredCountries = this.__self.filterCountries(this.lastText);

        const visibleCountries = Arrays.diff(this.filteredCountries, this.selectedCountries)
        if (visibleCountries.length === 1) {
            const name = visibleCountries[0];
            this.selectCountry(name);
            return;
        }

        this.redraw();
    }
},
{
    onInit() {
        this.initCountriesData();
        this._domEvents('country').on('click', this.prototype._onClick);
    },

    initCountriesData() {
        this._names = []; //список названий всех стран
        this._domElems = {}; //заранее отрисованные DOM элементы для каждой страны
        this._search = {}; //варианты соответствия текстового поиска странам
        this._namesMatched = {}; //подмножество ранее офильтрованных стран по первым двум буквам

        var bemjson = [];
        CountriesData.getAllCountriesGroups().forEach( function(country) {
            const name = country.name;

            this._names.push(name);
            this._search[name] = SoftMatch.makeSoft(country.search);
            bemjson.push({
                block: 'countries-select', elem: 'country',
                elemMods: { group: country.group },
                content: name
            });
        }.bind(this));

        const tempElem = document.createElement('div'); //нужен именно DOM element, т.к. DOM fragment не имеет свойства innerHTML
        tempElem.innerHTML = BEMHTML.apply(bemjson);

        const domElems = Array.prototype.slice.call(tempElem.childNodes);
        this._names.forEach(function (name, i) {
            this._domElems[name] = domElems[i];
        }.bind(this));

        this._namesMatched[''] = this._names;
    },

    replaceChildren(parentElem, items) {
        var fragment = document.createDocumentFragment();

        items.forEach( function(name) {
            const elem = this._domElems[name];
            elem && fragment.appendChild(elem);
        }.bind(this));

        DOM.replaceChildren(parentElem, fragment);
    },

    filterCountries(text) {
        const query = text.trim().toLowerCase();

        //кешируем результаты всех предыдущих запросов из одной буквы
        const subquery1 = query.substr(0, 1);
        if (!this._namesMatched[subquery1]) {
            this._namesMatched[subquery1] = this._match(subquery1, this._namesMatched['']);
        }

        //кешируем результаты всех предыдущих запросов из двух букв
        const subquery2 = query.substr(0, 2);
        if (!this._namesMatched[subquery2]) {
            this._namesMatched[subquery2] = this._match(subquery2, this._namesMatched[subquery1]);
        }

        //оптимизация простейшего случая
        if (query == subquery2) {
            return Arrays.clone(this._namesMatched[subquery2]);
        }

        return this._match(query, this._namesMatched[subquery2]);
    },

    _match(query, names) {
        const softQuery = SoftMatch.makeSoft(query);

        return names.filter( function(name) {
            return SoftMatch.matchStrict(softQuery, this._search[name]);
        }.bind(this));
    }

}
));
}
);
