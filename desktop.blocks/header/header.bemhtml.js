block('header')(
    tag()('header'),
    mix()('clearfix'),
    content()(function () {
        return {
            elem: 'inner',
            content: [
                {
                    mix: { block: 'main-menu', elem: 'icon' },
                    elem: 'menu-icon',
                    attrs: { role: 'button' },
                    content: {
                        block: 'icon',
                    }
                },
                {
                    mix: { block: 'header', elem: 'logo' },
                    block: 'link',
                    url: this.ctx.url,
                },
                {
                    mix: { block: 'header', elem: 'phone' },
                    block: 'link',
                    url: this.ctx.tel,
                    content: {
                        block: 'icon',
                    }
                }
            ]
        };
    })
);
