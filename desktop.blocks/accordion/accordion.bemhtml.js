block('accordion').content()(function() {
    return this.ctx.content.map( function(item) {
        return {
            block: 'accordion-item',
            title: item.title,
            content: item.content
        };
    });
});
