block('main-menu').elem('content')(
    tag()('nav'),
    content()(function() {
        return {
            elem: 'list',
            tag: 'ul',
            content: this.ctx.content.map( function (item) {
                return {
                    block: 'menu-item',
                    mix: { block: 'main-menu', elem: 'item' },
                    tag: 'li',
                    content: {
                        block: 'link',
                        mix: { block: 'main-menu', elem: 'link' },
                        url: item.url,
                        content: item.content
                    }
                };
            })
        }
    })
);
