modules.define('countries-selected', ['countries-data', 'arrays', 'dom', 'BEMHTML', 'i-bem-dom'],
function(provide, CountriesData, Arrays, DOM, BEMHTML, bemDom) {
provide(bemDom.declBlock(this.name,
{
    onSetMod: {
        js: {
            inited: function () {
                this.items = [];
            }
        }
    },

    render(codes) {
        const items = (codes || []).map(CountriesData.getName);

        if (Arrays.equal(items, this.items)) {
            return;
        }

        this.items = items;
        this.redraw();
    },

    redraw() {
        var bemjson = this._template(this.items);
        this.domElem.get(0).innerHTML = BEMHTML.apply(bemjson); //bemDom.update(this.domElem, BEMHTML.apply(bemjson));
    },

    _template(items) {
        var blockName = this.__self._blockName;
        var elemName = 'item';
        return items.map( function (item) {
            return {
                block: blockName,
                elem: elemName,
                content: item
            };
        }.bind(this));
    },

    _onClick(e) {
        const domElem = e.bemTarget.findParentElem('item').domElem.get(0);
        this._remove(domElem);
    },

    _remove(domElem) {
        var item = domElem.textContent.trim();

        var i = this.items.indexOf(item);
        if (i === -1) {
            return;
        }

        this.items.splice(i, 1);
        DOM.remove(domElem);
        const code = CountriesData.getCode(item);
        this._emit('remove', code);
    }
},
{
    onInit() {
        this._domEvents('control').on('click', this.prototype._onClick);
    }
}
));
}
);
