block('footer')(
    tag()('footer'),
    mix()({ block: 'view', elem: 'footer'}),
    content()([
        {
            elem: 'full-version',
            tag: 'a',
            attrs: {href: 'https://cherehapa.ru/'},
            content: [
                {
                    block: 'icon',
                    mix: {block: 'footer', elem: 'icon'}
                },
                {
                    tag: 'span',
                    content: 'Полная версия'
                }
            ]
        }
    ])
);
