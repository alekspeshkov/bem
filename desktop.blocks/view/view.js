modules.define('view', ['i-bem-dom'],
function(provide, bemDom) {
provide(bemDom.declBlock(this.name, {

    onSetMod: {
        js: {
            inited: function () {
                //TODO: перенести в статические методы
                var Page = bemDom.declBlock('page', {});
                var Header = bemDom.declBlock('header', {});
                this.page = this.findParentBlock(Page);
                this.header = this.page && this.page.findChildBlock(Header);
            }
        }
    },

    beforeSetMod: {
        visible: {
            'true': function () {
                //цвет фона и прочие стили
                this.page && this.page.setMod('theme', (this.params.theme || false));

                //по умолчанию header показываем
                this.header && this.header.setMod('hidden', (this.params.header === false));
            }
        }
    },

    getUrl: function() {
        return this.params.url;
    }

}));
}
);
