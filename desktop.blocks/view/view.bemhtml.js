block('view')(
    tag()('article'),

    js()(function() {
        let params = {};

        ['url', 'title', 'header', 'theme'].forEach( function (param) {
            if (this.ctx[param] != null) {
                params[param] = this.ctx[param];
            }
        }.bind(this));

        return params;
    }),

    addAttrs()(function() {
        let attrs = {};
        ['role'].forEach( function (attr) {
            if (this.ctx[attr] != null) {
                attrs[attr] = this.ctx[attr];
            }
        }.bind(this));
        return attrs;
    })
)

block('view').mix()('clearfix');
block('view').elem('twin').mix()('clearfix');
