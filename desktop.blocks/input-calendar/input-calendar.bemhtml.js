block('input-calendar')(
    content()(function() {
        return {
            mix: { block: this.ctx.block, elem: 'picker' },
            block: 'input', mods: { 'has-calendar': true, readonly: true },
            placeholder: this.ctx.placeholder || this.ctx.label || 'Дата',
            weekdays: this.ctx.weekdays || ['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс'],
            months: this.ctx.months || ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь']
        };
    })
);
