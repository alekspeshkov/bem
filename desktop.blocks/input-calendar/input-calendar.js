modules.define('input-calendar', ['input', 'i-bem-dom'],
function(provide, InputCalendar, bemDom) {
provide(bemDom.declBlock(this.name, {

    onSetMod: {
        js: {
            inited: function () {
                this._input = this.findChildBlock(InputCalendar);

                this._events(this._input).on('change', function () {
                    var val = this._input.getVal()
                    this._emit('select', val == '' ? null : val);
                }.bind(this));
            }
        }
    },

    render: function (datepicker) {
        this._input.setMod('disabled', datepicker.disabled === true);
        this._input.setVal(datepicker.select || '');
        this._input._calendar.setLimits(datepicker.min, datepicker.max);
    }

}));
}
);
