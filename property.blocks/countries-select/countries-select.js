modules.define('countries-select', ['property'],
function(provide, Property, Base) {
provide(Base.declMod({ modName: 'prop', modVal: '*' },
{
    onSetMod: {
        js: {
            inited: function() {
                this.__base.apply(this, arguments);

                this.property = new Property(
                    this.getMod('prop'),
                    this.render.bind(this)
                );

                this._events().on('select', function (e, item) {
                    this.property.addItem(item);
                });
            }
        }
    }
}
));
}
);
