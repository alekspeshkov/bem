modules.define('input-calendar', ['property'],
function(provide, Property, Base) {
provide(Base.declMod({ modName: 'prop', modVal: '*' }, {

    onSetMod: {
        js: {
            inited: function () {
                this.__base.apply(this, arguments);

                this.property = new Property(
                    this.getMod('prop'),
                    this.render.bind(this)
                );

                this._events().on('select', function (e, date) {
                    this.property.set(date);
                });
            }
        }
    }

}));
}
);
