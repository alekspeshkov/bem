modules.define('checkbox', ['property'],
function(provide, Property, Base) {
provide(Base.declMod({ modName: 'prop', modVal: '*' }, {

    onSetMod: {
        js: {
            inited: function() {
                this.__base.apply(this, arguments);

                this.property = new Property(
                    this.getMod('prop'),
                    function (checked) {
                        this.setMod('checked', checked);
                    }.bind(this)
                );

                this._events().on({ modName: 'checked', modVal: '*' }, function() {
                    const checked = this.hasMod('checked');
                    this.property.set(checked);
                });
            }
        }
    }

}));
}
);
