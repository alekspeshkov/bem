modules.define('property', ['request-bus', 'response-bus'],
function(provide, requestBus, responseBus) {

    class Property {
        isInited = false;

        constructor(name, onChange) {
            this.key = name; //имя свойства модели, за которым наблюдает и изменяет презентер
            this._onChange = onChange;
            //подписываемся на получение событий из шины response об изменении свойств модели
            this._responseBus.subscribe(this._onResponse.bind(this), [this.key]);
            /*
             * команда ping запрашивает начальное состояние свойств модели
             * асинхронно получив первый ответ от модели, презентер устанавливает this.isInited = true
             */
            this._requestBus.send({ action: 'ping', value: [this.key] });
        }

        set(value) {
            if (!this.isInited) {
                //не спамим командами шину пока не уверены, что нас уже слушают
                console.warn('skipped set property', this.key, value);
                return;
            }
            this._requestBus.send({ action: 'set', key: this.key, value: value });
        }

        addItem(item) {
            if (!this.isInited) {
                console.warn('skipped add item property', this.key, item);
                return;
            }
            this._requestBus.send({ action: 'add', key: this.key, value: item });
        }

        removeItem(item) {
            if (!this.isInited) {
                console.warn('skipped remove item', this.key, item);
                return;
            }
            this._requestBus.send({ action: 'remove', key: this.key, value: item });
        }

        _onResponse(response) {
            //начальная отрисовка должна произойти независимо от наличия каких-то изменений
            if (!this.isInited) {
                this.isInited = true;
                this._onChange(response.state[this.key]);
                return;
            }
            //реагируем только при фактическом изменения наблюдаемых свойств
            if (response.changedKeys.indexOf(this.key) !== -1) {
                this._onChange(response.state[this.key]);
                return;
            }
        }
    }

    Property.prototype._requestBus = requestBus;
    Property.prototype._responseBus = responseBus;

    provide(Property);
}
);
