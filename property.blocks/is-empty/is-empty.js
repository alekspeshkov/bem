modules.define('is-empty', ['i-bem-dom'],
function(provide, bemDom) { provide(bemDom.declBlock(this.name, {
    //пусто
}));
}
);

modules.define('is-empty', ['property', 'types'],
function(provide, Property, Types, Base) {
provide(Base.declMod({ modName: 'prop', modVal: '*' }, {

    onSetMod: {
        js: {
            inited: function() {
                this.__base.apply(this, arguments);

                this.property = new Property(
                    this.getMod('prop'),
                    function (value) {
                        this.setMod('no', !Types.isEmpty(value));
                    }.bind(this)
                );
            }
        }
    }

}));
}
);
