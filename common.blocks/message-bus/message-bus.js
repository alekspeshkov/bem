modules.define('message-bus', function(provide) {

    /**
     * Задача шины отправлять всем подписчикам все приходящие сообщения, изолируя отправителей и получателей
     */
    class MessageBus {
        subscribers = [];

        /**
         * Отправка сообщения всем ранее зарегистированным подписчикам
         **/
        send(...args) {
            this.subscribers.forEach(function (subscriber) {
                //все подписчики уведомляются параллельно
                setTimeout(function () {
                    subscriber.apply(undefined, args);
                }, 0);
            });
        }

        /**
         * Регистрация подписчика
         *
         * @param subscriber function(message) функция-получатель сообщения
         * @return ключ данного подписчика, по которому можно потом отписать себя
         **/
        subscribe(subscriber) {
            const key = this.subscribers.push(subscriber) - 1;
            return key;
        }

        /**
         * отмена регистрации подписчика
         **/
        unsubscribe(key) {
            // заменяем функцию подписки на пустую, чтобы индексы массива не изменились
            this.subscribers[key] = function () { };
        }
    }

    provide(MessageBus);

});
