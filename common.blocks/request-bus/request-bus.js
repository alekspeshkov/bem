/**
 * Глобальная очередь запросов из view в models
 * Формат запроса:
 * { action: action, key: key, value: value }
 */
modules.define('request-bus', ['message-bus'],
function (provide, MessageBus) {
provide(

    new MessageBus('requestBus')

);
}
);
