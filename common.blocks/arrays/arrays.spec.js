modules.define('spec', [ 'arrays', 'chai'],
function (provide, Arrays, chai) {
    var expect = chai.expect;

    describe('Arrays.clone', function() {
        it('should work', function() {
            [
                [[], []],
                [['a', 'b', 'c'], ['a', 'b', 'c']],
            ]
                .forEach( function (test) {
                    expect( Arrays.clone(test[0]) ).deep.equal(test[1]);
                });

        });
    });

    describe('Arrays.diff', function() {
        it('should work', function() {
            [
                [[], [], []],
                [[], ['b', 'c'], []],
                [['a', 'b', 'c'], [], ['a', 'b', 'c']],
                [['a', 'b', 'c'], ['b', 'c'], ['a']],
                [['a', 'b', 'c'], ['d', 'c', 'a'], ['b']],
                [['a', 'b', 'c'], ['d', 'e', 'f'], ['a', 'b', 'c']]
            ]
                .forEach( function (test) {
                    expect( Arrays.diff(test[0], test[1]) ).deep.equal(test[2]);
                });

        });
    });

    describe('Arrays.only', function() {
        it('should work', function() {
            [
                [[], [], []],
                [[], ['b', 'c'], []],
                [['a', 'b', 'c'], [], []],
                [['a', 'b', 'c'], ['c', 'b'], ['b', 'c']],
                [['a', 'b', 'c'], ['d', 'c', 'a'], ['a', 'c']],
                [['a', 'b', 'c'], ['d', 'e', 'f'], []]
            ]
                .forEach( function (test) {
                    expect( Arrays.only(test[0], test[1]) ).deep.equal(test[2]);
                });

        });
    });

    describe('Arrays.merge', function() {
        it('should work', function() {
            [
                [[], [], []],
                [[], ['b', 'c'], ['b', 'c']],
                [['a', 'b', 'c'], [], ['a', 'b', 'c']],
                [['a', 'b', 'c'], ['c', 'b'], ['a', 'b', 'c']],
                [['a', 'b', 'c'], ['d', 'c', 'a'], ['a', 'b', 'c', 'd']],
                [['a', 'b', 'c'], ['d', 'e', 'f'], ['a', 'b', 'c', 'd', 'e', 'f']]
            ]
                .forEach( function (test) {
                    expect( Arrays.merge(test[0], test[1]) ).deep.equal(test[2]);
                });

        });
    });

    describe('Arrays.pushUnique', function() {
        it('should work', function() {
            [
                [[], 'a', ['a']],
                [['a'], 'b', ['a', 'b']],
                [['a', 'b', 'c'], 'b', ['a', 'b', 'c']]
            ]
                .forEach( function (test) {
                    expect( Arrays.pushUnique(test[0], test[1]) ).deep.equal(test[2]);
                });

        });
    });

    describe('Arrays.remove', function() {
        it('should work', function() {
            [
                [[], 'a', []],
                [['a'], 'b', ['a']],
                [['a', 'b', 'c'], 'b', ['a', 'c']]
            ]
                .forEach( function (test) {
                    expect( Arrays.remove(test[0], test[1]) ).deep.equal(test[2]);
                });

        });
    });

    describe('Arrays.indexOf', function() {
        it('should work', function() {
            [
                [[], 'b', 'i', -1],
                [[{i:'a'}, {i:'b'}, {i:'c'}], 'a', 'i', 0],
                [[{i:'a'}, {i:'b'}, {i:'c'}], 'b', 'i', 1],
                [[{i:'a'}, {i:'b'}, {i:'c'}], 'd', 'i', -1],
                [[{i:'a'}, {i:'b'}, {i:'c'}], 'a', 'k', -1],
                [[], 'b', undefined, -1],
                [['a', 'b', 'c'], 'a', undefined, 0],
                [['a', 'b', 'c'], 'b', undefined, 1],
                [['a', 'b', 'c'], 'd', undefined, -1],
            ]
                .forEach( function (test) {
                    expect( Arrays.indexOf(test[0], test[1], test[2]) ).equal(test[3]);
                });

        });
    });

    describe('Arrays.findIndex', function() {
        it('should work', function() {
            [
                [[], function() { return false; }, undefined, -1],
                [[], function() { return true; }, undefined, -1],
                [[5], function() { return true; }, undefined, 0],
                [[0, 1, 2, 3], function(value) { return value === 2; }, undefined, 2],
                [[2, 3, 5, 8], function(value, i, array) {
                    expect(value).equal(array[i]);
                    return value === 3;
                }, undefined, 1],
            ]
                .forEach( function (test) {
                    expect( Arrays.findIndex(test[0], test[1], test[2]) ).equal(test[3]);
                });

        });
    });

    describe('Arrays.equal', function() {
        it('should work', function() {
            [
                [[], [], true],
                [[1, 2], [1, 2], true],
                [[1, 2], [1, 2, 3], false],
                [[1, 2], [2, 1], false],
            ]
                .forEach( function (test) {
                    expect( Arrays.equal(test[0], test[1]) ).equal(test[2]);
                });

        });
    });

    describe('Arrays.uniqueSort', function() {
        it('should work', function() {
            [
                [[], []],
                [[1, 2], [1, 2]],
                [[2, 1], [1, 2]],
                [[2, 2, 1, 2, 1], [1, 2]],
            ]
                .forEach( function (test) {
                    expect( Arrays.uniqueSort(test[0]) ).deep.equal(test[1]);
                });

        });
    });

    provide();

});
