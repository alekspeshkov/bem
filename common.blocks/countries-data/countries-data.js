modules.define('countries-data', [],
function(provide) {
    var countriesRaw = [/*borschik:include:../../common.blocks/countries-data/countries.json*/];

    var countriesGroups = [];
    var countriesCode = {};
    var countriesName = {};

    countriesRaw.forEach( function(country) {
        var countryName = country.name;

        countriesGroups.push({
            name: countryName,
            group: (country.group === 'group'),
            search: country.search
        });
        countriesCode[countryName] = country.id;
        countriesName[country.id] = countryName;
    });

    var CountriesData = {
        getAllCountriesGroups: function() {
            return countriesGroups;
        },

        getName: function(countryCode) {
            return countriesName[countryCode];
        },

        getCode: function(countryName) {
            return countriesCode[countryName];
        }

    };

    provide(CountriesData);

});