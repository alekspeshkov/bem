modules.define('storage', ['message-bus', 'objects'],
function(provide, MessageBus, Objects) {

    const clone = Objects.clone;

    class Storage {
        _state = {};

        constructor() {
            this._bus = new MessageBus('storage');
        }

        cloneState() {
            return clone(this._state);
        }

        /**
         * Для каждого одноимённого собственного свойства из object
         * создаём или полностью заменяем свойства внутреннего состояния _state
         **/
        updateState(object) {
            const nextState = this.cloneState();

            let changedKeys = [];
            Objects.each(object, function (value, key) {
                if (nextState[key] !== value) {
                    nextState[key] = clone(value);
                    changedKeys.push(key);
                }
            });

            /**
             * Оптимизация: если фактических изменений не было,
             * то никаких событий мы не отправляем
             **/
            if (!changedKeys.length) {
                return;
            }

            this._state = nextState;
            this._bus.send(this.cloneState(), changedKeys);
        }

        pingState() {
            this._bus.send(this.cloneState(), []);
        }

        subscribe(subscriber) {
            this._bus.subscribe(subscriber);
        }

    }

    provide(new Storage);

});
