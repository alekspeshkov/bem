modules.define('objects',
function(provide, Objects) {

    var clone = function(object) {
        try {
            return (object === undefined) ? undefined : JSON.parse(JSON.stringify(object));
        }
        catch (e) {
            console.warn('игнорируем exception внутри Objects.clone()');
            return undefined;
        }
    };

    var only = function(object, keys) {
        var result = {};

        keys.forEach( function(key) {
            if (object[key] != null) {
                result[key] = clone(object[key]);
            }
        });

        return result;
    };

    var flip = function(object) {
        var newObj = {};
        Object.keys(object).forEach( function(elem) {
            newObj[object[elem]] = elem;
        });
        return newObj;
    };

    provide(Objects.extend(Objects, { clone: clone, only: only, flip: flip }));

}
);
