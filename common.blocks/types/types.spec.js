modules.define('spec', [ 'types', 'chai'],
function (provide, Types, chai) {
    var assert = chai.assert;

    describe('Types', function() {

        it('isInteger', function() {
            [0, 1, -1, 100500, -100500, 1e6, 100.0]
            .forEach( function(n) {
                assert.equal(Types.isInteger(n), true, 'isInteger: ' + n);
            });

            [
                null, undefined, true, false, /Kit/g, function(){},
                '', '0', '1', '01.01.1970', 'Черехапа',
                0.5, -0.001, NaN, -NaN, Infinity, -Infinity,
                new Date(), {}, [], [0], [0, 1], { val: 1 }
            ]
            .forEach( function(n) {
                assert.equal(Types.isInteger(n), false, 'notInteger: ' + n);
            });
        });

        it('isFunction', function() {
            [
                function(){},
                {}.toString,
                {}.hasOwnProperty,
                alert,
                Object,
                Array,
                Date,
                String,
            ]
                .forEach( function(f) {
                    assert.equal(Types.isFunction(f), true, 'Types.isFunction(): ' + f);
                });

            [
                /Kit/g,
                null,
                undefined,
                {},
                [],
                [].length,
                new Object,
                new Array,
                new Date,
                new String,
                0,
                +100500.02,
                '',
                'abcdefghijklmnopqrstuvwxyz0123456789'
            ]
                .forEach( function(f) {
                    assert.equal(Types.isFunction(f), false, 'not Types.isFunction(): ' + f);
                });
        });

        it('isEmpty', function() {
            [
                null,
                undefined,
                0,
                false,
                '',
                0.0,
                [],
                {}
            ]
                .forEach( function(v) {
                    assert.equal(Types.isEmpty(v), true, 'Types.isEmpty(): ' + v);
                });

            [
                true,
                function(){},
                { a: 0 },
                [ 0 ],
                -1,
                '0',
                'xxx',
            ]
                .forEach( function(v) {
                    assert.equal(Types.isEmpty(v), false, 'not Types.isEmpty(): ' + v);
                });
        });

    });

    provide();

});
