modules.define('types', ['objects'],
function (provide, Objects) {

    var Types = {};

    Types.toInteger = function (n) {
        return n >= 0 ? Math.floor(n) : Math.ceil(n);
    };

    Types.isInteger = function (n) {
        return (typeof n === 'number')
            && isFinite(n)
            && (Types.toInteger(n) === n);
    };

    Types.isFunction = function (obj) {
        return !!(obj && obj.constructor && obj.call && obj.apply);
    };

    Types.isEmpty = function (value) {

        if (value == null) {
            return true;
        }

        if (Array.isArray(value)) {
            return value.length === 0;
        }

        //BEM collections and similar objects
        if (value.toArray && Types.isFunction(value.toArray)) {
            return value.toArray().length === 0;
        }

        if (Types.isFunction(value)) {
            return false;
        }

        if (Object(value) === value) {
            return Objects.isEmpty(value);
        }

        return !value;
    };

    provide(Types);

}
);
