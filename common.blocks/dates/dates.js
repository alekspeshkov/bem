modules.define('dates', ['types'],
function (provide, Types) {

    var Dates = {};

    Dates.defaultFormat = function () {
        return 'dd.mm.yyyy';
    };

    Dates.formatDate = function (dateable, toISO) {
        var date = Dates.parseDate(dateable);
        if (!date) {
            throw 'Не задана дата';
        }

        var yyyy = date.getUTCFullYear();
        var mm = date.getUTCMonth() + 1; //January is 0!
        var dd = date.getUTCDate();

        if (dd < 10) {
            dd = '0' + dd
        }

        if (mm < 10) {
            mm = '0' + mm
        }

        return toISO
            ? yyyy + '-' + mm + '-' + dd
            : dd + '.' + mm + '.' + yyyy;
    };

    //преобразует момент времени на начало календарного дня (0 часов 00 минут в UTC timezone)
    Dates.parseDate = function (dateable) {
        if (dateable == null) {
            throw 'Не задана дата';
        }

        var date = Types.isInteger(dateable) ? new Date(dateable) : dateable;

        if (date instanceof Date) {
            //нормализуем дату
            return new Date(Date.UTC(
                date.getUTCFullYear(),
                date.getUTCMonth(),
                date.getUTCDate()
            ));
        }

        //pickadate format
        if (typeof date === 'object' &&
            Types.isInteger(date.year) &&
            Types.isInteger(date.month) &&
            Types.isInteger(date.date)) {

            return new Date(Date.UTC(
                date.year,
                date.month,
                date.date
            ));
        }

        var DMY = /\d{1,2}\.\d{1,2}\.\d{2,4}/;
        var ISO = /\d{2,4}-\d{1,2}-\d{1,2}/;
        var MDY = /\d{1,2}\/\d{1,2}\/\d{2,4}/;
        var yyyy, mm, dd;

        if (DMY.test(date)) {
            var dateSplit = date.split('.');
            dd = dateSplit[0];
            mm = dateSplit[1];
            yyyy = dateSplit[2];
        }
        else if (ISO.test(date)) {
            var dateSplit = date.split('-');
            yyyy = dateSplit[0];
            mm = dateSplit[1];
            dd = dateSplit[2];
        }
        else if (MDY.test(date)) {
            var dateSplit = date.split('/');
            mm = dateSplit[0];
            dd = dateSplit[1];
            yyyy = dateSplit[2];
        }
        else {
            var timestamp = Date.parse(date);

            if (isNaN(timestamp)) {
                console.error('Неизвестный формат даты', date);
                throw 'Неизвестный формат даты';
            }

            var newDate = new Date(timestamp);

            return new Date(Date.UTC(
                newDate.getUTCFullYear(),
                newDate.getUTCMonth(),
                newDate.getUTCDate()
            ));
        }

        if (31 < yyyy && yyyy < 100) {
            yyyy = 1900 + +yyyy;
        }

        return new Date(Date.UTC(yyyy, mm - 1, dd));
    };

    //возвращает разницу между датами в полных днях
    Dates.diffDays = function (dateableEnd, dateableStart) {
        var dateEnd = Dates.parseDate(dateableEnd);
        var dateStart = Dates.parseDate(dateableStart);

        var msecsPerDay = 24 * 60 * 60 * 1000;
        var days = (dateEnd.getTime() - dateStart.getTime()) / msecsPerDay;

        return Math.round(days);
    },

    //возвращает разницу между датами в полных годах
    Dates.diffYears = function (dateableEnd, dateableStart) {
        var dateEnd = Dates.parseDate(dateableEnd);
        var dateStart = Dates.parseDate(dateableStart);

        var years = dateEnd.getUTCFullYear() - dateStart.getUTCFullYear();

        var monthDiff = dateEnd.getUTCMonth() - dateStart.getUTCMonth();
        var daysDiff = dateEnd.getUTCDate() - dateStart.getUTCDate();
        if (monthDiff < 0 || (monthDiff == 0 && daysDiff < 0)) {
            return years - 1;
        }

        return years;
    };

    /**
     * Возвращает начало календарного дня (0:00 UTC) для момента времени в указанном часовом поясе
     * @param {Number} timezoneOffset (в минутах)
     * @param {Date} date произвольный момент времени, по умолчанию: прямо сейчас
     * @returns {String} Formatted date
     */
    Dates.calendarDate = function(offset, date) {
        if (date == null) {
            date = new Date();
        }

        var localTime = date.getTime();
        var offsetMilliseconds = offset * 60 * 1000;
        var resultDate = new Date(localTime - offsetMilliseconds);
        return Dates.formatDate(resultDate);
    };

    Dates.plusDays = function(dateable, days) {
        var date = Dates.parseDate(dateable);
        date.setUTCDate(date.getUTCDate() + days);
        return Dates.formatDate(date);
    };

    Dates.plusYear = function (dateable) {
        var date = Dates.parseDate(dateable);
        date.setUTCFullYear(date.getUTCFullYear() + 1);
        return Dates.formatDate(date);
    };

    provide(Dates);

}
);
