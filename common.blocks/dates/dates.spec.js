modules.define('spec', ['dates', 'chai'],
function (provide, Dates, chai) {
    var assert = chai.assert;

    describe('dates', function() {

        it('formatDate', function() {
            assert.equal(Dates.formatDate('11.09.2001'), '11.09.2001', 'date equal to date');
            assert.equal(Dates.formatDate(0), '01.01.1970', 'start of epoch');
            assert.equal(Dates.formatDate('1.2.34'), '01.02.1934', 'short year');
            assert.equal(Dates.formatDate('9/11/2001'), '11.09.2001', 'american year');
        });

        it('diffDays', function() {
            assert.equal(Dates.diffDays('07 Nov 2016', '07 Nov 2016'), 0, 'same dates diff');
            assert.equal(Dates.diffDays('07 Nov 2016', '06 Nov 2016'), 1, 'date equal to date');
            assert.equal(Dates.diffDays('06 Nov 2016', '07 Nov 2016'), -1, 'date equal to date');
            assert.equal(Dates.diffDays('07 Nov 2016', '07 Nov 2015'), 366, 'leap year diff');
            assert.equal(Dates.diffDays('07 Nov 2016', '07 Nov 2017'), -365, 'common year diff');
        });

        it('diffYears', function() {
            assert.equal(Dates.diffYears('07 Nov 2016', '07 Nov 2016'), 0, 'same dates diff');
            assert.equal(Dates.diffYears('07 Nov 2017', '07 Nov 2016'), +1, 'full year');
            assert.equal(Dates.diffYears('07 Nov 2016', '07 Nov 2017'), -1, 'full year');
            assert.equal(Dates.diffYears('07 Oct 2017', '07 Nov 2016'), 0, 'less then year');
            assert.equal(Dates.diffYears('07 Nov 2017', '07 Oct 2016'), +1, 'more then year');
            assert.equal(Dates.diffYears('06 Nov 2017', '07 Nov 2016'), 0, 'less then year');
            assert.equal(Dates.diffYears('08 Nov 2017', '07 Nov 2016'), +1, 'more then year');
            assert.equal(Dates.diffYears('2016-02-29', '2015-02-28'), +1, 'leap year diff');
            assert.equal(Dates.diffYears('2016-02-29', '2017-02-28'), -1, 'leap year diff');
            assert.equal(Dates.diffYears('2016-02-28', '2015-02-28'), 1, 'leap year diff');
            assert.equal(Dates.diffYears('2016-02-28', '2017-02-28'), -1, 'leap year diff');
        });

        it('plusYear', function() {
            assert.equal(Dates.plusYear(0), '01.01.1971', 'happy new year');
            assert.equal(Dates.plusYear('29.02.1972'), '01.03.1973', 'leap year');
        });

        it('calendarDate', function() {
            var offset = -180;
            assert.equal(Dates.calendarDate(offset, new Date('07 Nov 2016  0:00 UTC+0300')), '07.11.2016', 'Moscow time');
            assert.equal(Dates.calendarDate(offset, new Date('07 Nov 2016 23:59 UTC+0300')), '07.11.2016', 'Moscow time');

            assert.equal(Dates.calendarDate(offset, new Date('07 Nov 2016  0:00 UTC-0500')), '07.11.2016', 'New-York time');
            assert.equal(Dates.calendarDate(offset, new Date('07 Nov 2016 23:59 UTC-0500')), '08.11.2016', 'New-York time');

            assert.equal(Dates.calendarDate(offset, new Date('07 Nov 2016  0:00 UTC+0700')), '06.11.2016', 'Novosibirsk time');
            assert.equal(Dates.calendarDate(offset, new Date('07 Nov 2016 23:59 UTC+0700')), '07.11.2016', 'Novosibirsk time');
        });

    });

    provide();

});
