#Мобильная версия фронтенда на БЭМ
**T L ; D R** Самое быстрое введение для программистов, которые про БЭМ раньше не слышали: https://ru.bem.info/platform/i-bem/overview/
(достаточно прочитать только первый раздел *"БЭМ-методология и JavaScript"*).

Чуть подробнее: https://ru.bem.info/methodology/key-concepts/

## Скачиваем проект и устанавливаем его зависимости
Мобильная версия не требует для себя запуск внутри **vagrant**. Предварительно необходимо установить себе **Node.js** https://nodejs.org/en/download/
```bash
git clone git@bitbucket.org:tsystem-ondemand/che-mobile.git
cd che-mobile
npm install
```
## Запускаем веб-сервер
```bash
npm start
```
Cмотреть в браузере по адресу:
http://0.0.0.0:8080 (в Windows по адресу http://127.0.0.1:8080/)

## БЭМ -- это абревиатура "Блок-Элемент-Модификатор"

### Блок -- это модуль в терминах БЭМ
* Все файлы реализации блока (JS, CSS, шаблоны, PNG/SVG и т.д.), как правило, лежат в одноимённой папке, плюс каждый файл в этой папке совпадает с именем блока, имена файлов блока отличаются только расширением (конец имени файла после первой точки слева) https://ru.bem.info/methodology/filestructure/
* Один блок может быть реализован в разных **технологиях** (технология определяется расширением файла). Все технологии опциональны. Сборщик проекта собирает каждую технологию независимо. Правила сборки всех технологий находятся в файле **.enb/make.js**
* Файлы одного блока могут быть расположен сразу в нескольких каталогах **(*.blocks/)**. Файлы блока собираются из всех каталогов. Если есть одноимённые файлы, то используются самые последняя версия (порядок приоритетов каталогов называется **уровнями переопределения**).
* Переопределение технологии **js** -- исключение из правила выше. В собранном проекте будут одновременно присутствовать все версии js, неоднозначности использования одноимённых js-модулей решаются модульной системой YModules. (https://habrahabr.ru/post/213627/)
* Если блок визуальный (т.е. привязан к конкретному DOM-элементу), то имя блока это значение атрибута class любого HTML-тега `<div class="block-name">`. https://ru.bem.info/platform/i-bem/html-binding/

### Элемент -- это принадлежащий другому блоку зависимый модуль
* Элемент всегда имеет определенный родительский блок. У элементов не может быть элементов.
* Если элемент сложный, то файлы его реализации можно положить в отдельную подпапку внутри папки своего родителького блока.
* Если элемент сложный, то его можно реализовать как независимый блок, а в родительском блоке создать микс элемента и нового блока.
* Если визуальный блок имеет элемент, то он записывается как `<div class="block-name__elem-name">`. Элемент неотделим от своего блока и поэтому находится в его пространстве имён.

### Модификатор -- это дискретное состояние блока или элемента
* Например, DOM-coстояние `input:checked` в БЭМ для удобства работы трансформируется в модификатор **.input_checked**
* Изменение видимого и/или внутреннего состояния блока в БЭМ реализуется как изменение модификатора. На установку, удаление или изменение значения модификатора удобно повесить события JS.
* Если визуальный блок имеет модификатор, то для блока с именем `block-name` мод записывается как `<div class="block-name block-name_mod_mod-value">` или просто `<div class="block block_mod">` (в JS значением такого упрощенного модификатора считается строка "true" или булево значение **true**)
* Модификатор элемента записывается как `<div class="block-name__elem-name_elem-mod_elem-mod-value">` или просто `<div class="block-name__elem-name_elem-mod">`

### Миксины
* На одном DOM-элементе могут располагаться любое число разных блоков и/или элементов и иметь произвольное кол-во модификаторов.

## Технологии реализации блоков

* **.deps.js** зависимости данного блока от других блоков.
Можно указать, чтобы зависимые блоки были расположены либо строго раньше в файле сборки (**mustDeps**, редкий случай), либо просто добавлены как угодно (**shouldDeps**).
https://ru.bem.info/platform/deps-spec/
* **.js** (в самих библиотеках БЭМ различают **.vanilla.js** и **.browser.js**, у нас в проекте всё просто **.js**) Модули js-кода в формате YModules. https://ru.bem.info/platform/i-bem/
* **.css**, **.styl** CSS-стили https://ru.bem.info/methodology/css/
* **.bemhtml.js** шаблоны генерации HTML-кода блока. Декларативны (порядок написанных деклараций не влияет на результат), поэтому выглядят несколько необычно. https://ru.bem.info/platform/bem-xjst/
* **.spec.js** код юнит-тестов для данного блока, тесты запускаются через `npm run specs` https://github.com/enb/enb-bem-specs

### Технология JavaScript в БЭМ
* Ядро БЭМ реализовано через jQuery и сама библиотека jQuery всегда доступна в коде.
* Если поведение блока неудобно реализовать через события изменения модификаторов, то можно использовать БЭМ-события.
* Как правило, в БЭМ принято браузерные DOM-события преобразовывать в БЭМ-события или события изменения модификаторов.

### Инициализация блоков с JS
* Блок логически существует еще до того, как выполниться его JS-код (технология JS-вообще опциональна).
* Инициализация JS-кода блоков может быть как автоматической (асинхронно сразу же после загрузки файлов `index.html` и `index.min.js`),
так и отложенной (**lazyInit**) по наступлению каких-то определенных DOM-событий (прописанных в статическом методе onInit() данного блока) или в момент первого использования данного блока исполняемым кодом другого блока.
* Ядро БЭМ при инициализации блока автоматически выставляет блоку модификатор `block-name_js_inited`. Соответственно, обработчик события **setMod('js', 'inited')** фактически выполняет функцию конструктора js-объекта экземпляра данного блока в БЭМ. А обработчик события **setMod('js','')** -- выполняется в момент уничтожения JS-объекта представляющего блок.
* Если у блока с DOM-представлением есть параметры передаваемые в его JS-код, то они указываётся в виде структуры в формате JSON внутри атрибута `data-bem` (`<div class="block-name" data-bem='{"block-name":{"lazyInit":false}}'>`

## В ДАННОМ ПРОЕКТЕ созданы следующие уровни переопределения блоков:

* **node_modules/bem-core/*.blocks** ядро фреймворка, самые базовые блоки, за редким исключением, не имеют представления в DOM https://ru.bem.info/platform/libs/bem-core/4.1.1/
* **node_modules/bem-components/*.blocks** библиотека визуальных блоков, реализует основные элементы веб-страницы, в частности унифицируют поведение разных браузеров https://ru.bem.info/platform/libs/bem-components/5.0.0/

* **legacy.blocks** БЭМ-обёртка (модули в формате YModules) над внешними модулями, которые инсталируются и лежат в **node_modules**
* **common.blocks** библиотеки функций общего назначения без визуальной специфики и бизнес-логики приложения
* **desktop.blocks** визуальные блоки ("десктоп" тут в смысле того, что эти версии блоков без специфики мобильных устройств)
* **touch.blocks**, **phone.blocks** особые версии блоков из desktop.blocks под мобильные платформы, если требуется (сейчас таких нет)

* **bus.blocks** реализация асинхронного создания и обмена данными между моделями и блоками отображения
* **property.blocks** адаптеры для блоков из desktop.blocks для работы с шиной обмена сообщениями с моделью данных специфической для данного проекта
* **model.blocks** модели данных приложения
* **development.blocks**, **production.blocks** альтернативные версии блоков для сборки

## Всё суть блоки, а кроме блоков есть ещё BEMJSON

Блок c именем **page** содержит каркас произвольной (пустой) HTML страницы, все остальные блоки добавляются внутрь него.
А вот сама информация, какие блоки должны быть на конкретной HTML-странице, содержится в файлах `*.bundles/имя_страницы/имя_страницы.bemjson.js`.
Это означает, что на полученной с сервера HTML-страницы уже **свёрстаны все блоки**, которые, если имеют собственный js-код, инициализируются независимо и все одновременно.

Так как у нас Single Page Application, то в нашем случае есть единственная HTML-страница и BEMJSON-файл в данном проекте всего один `desktop.bundles/index/index.bemjson.js`.
В этом файле упомянуты все блоки (и параметры этих блоков). Иерархия вложенности блоков в файле BEMJSON изоморфно преобразуется в самый обычный HTML-код.
Все упомянутые в BEMJSON блоки автоматически становятся зависимостями, которые добавляются в проект генерации данной страницы.
Все остальные зависимости одних блоков от других блоков неавтоматические и явно записываются в файл **block-name.deps.js**
https://ru.bem.info/platform/bemjson/

## Документация
* http://ru.bem.info/ Официальный сайт с документацией и форум.
* Файл README.stub.ru.md -- README из **bem/project-stub** (на базе которого развернут данный проект). В частности, там есть полезные конкретные ссылки на информацию по БЭМ.
